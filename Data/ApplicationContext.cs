using Microsoft.EntityFrameworkCore;
using Models.AdminModel;

namespace Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            
        }
        public DbSet<CompanySetting> CompanySettingsDB {get; set;}
        public DbSet<Bannar> BannarDB {get; set;}
        public DbSet<Models.AdminModel.Product> Product { get; set; }
        public DbSet<Models.AdminModel.ProductFeature> ProductFeature { get; set; }
    }
}