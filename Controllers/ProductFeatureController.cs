using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data;
using Models.AdminModel;

namespace Adminpanel.Controllers
{
    public class ProductFeatureController : Controller
    {
        private readonly ApplicationContext _context;

        public ProductFeatureController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: ProductFeature
        public async Task<IActionResult> Index()
        {
            var applicationContext = _context.ProductFeature.Include(p => p.Product);
            return View(await applicationContext.ToListAsync());
        }

        // GET: ProductFeature/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productFeature = await _context.ProductFeature
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productFeature == null)
            {
                return NotFound();
            }

            return View(productFeature);
        }

        // GET: ProductFeature/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Id");
            return View();
        }

        // POST: ProductFeature/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FeatureName,ProductId")] ProductFeature productFeature)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productFeature);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Id", productFeature.ProductId);
            return View(productFeature);
        }

        // GET: ProductFeature/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productFeature = await _context.ProductFeature.FindAsync(id);
            if (productFeature == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Id", productFeature.ProductId);
            return View(productFeature);
        }

        // POST: ProductFeature/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FeatureName,ProductId")] ProductFeature productFeature)
        {
            if (id != productFeature.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productFeature);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductFeatureExists(productFeature.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Id", productFeature.ProductId);
            return View(productFeature);
        }

        // GET: ProductFeature/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productFeature = await _context.ProductFeature
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productFeature == null)
            {
                return NotFound();
            }

            return View(productFeature);
        }

        // POST: ProductFeature/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productFeature = await _context.ProductFeature.FindAsync(id);
            _context.ProductFeature.Remove(productFeature);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductFeatureExists(int id)
        {
            return _context.ProductFeature.Any(e => e.Id == id);
        }
    }
}
