using System;
using System.IO;
using System.Threading.Tasks;
using Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.AdminModel;

namespace Controllers
{
    public class AdminController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly IWebHostEnvironment _WebHostEnvironment;

        public AdminController(ApplicationContext context, IWebHostEnvironment WebHostEnvironment)
        {
            _context = context;
            _WebHostEnvironment = WebHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.CompanySettingsDB.ToListAsync());
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AdminViewModel vm)
        {
            string FileName = UploadFile(vm);
            var models = new CompanySetting
            {
                CompanyName = vm.CompanyName,
                Address = vm.Address,
                PhoneNumber = vm.PhoneNumber,
                Email = vm.Email,
                Logo = FileName
            };


            if (ModelState.IsValid)
            {
                _context.Add(models);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        ////////////////
        ///image file 
        ///////////////
        private string UploadFile(AdminViewModel vm)
        {
            string fileName = null;

            if (vm.Logo != null)
            {
                string uploadDir = Path.Combine(_WebHostEnvironment.WebRootPath, "images");
                fileName = Guid.NewGuid().ToString() + "-" + vm.Logo.FileName;
                string filePath = Path.Combine(uploadDir, fileName);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    vm.Logo.CopyTo(fileStream);
                }
            }
            return fileName;
        }





        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.CompanySettingsDB.FindAsync(id);

            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = _context.CompanySettingsDB.Find(id);
            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(int id, CompanySetting models)
        {
            if (id != models.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _context.Update(models);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(models);

        }




        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var students = await _context.CompanySettingsDB
                .FirstOrDefaultAsync(m => m.Id == id);
            if (students == null)
            {
                return NotFound();
            }

            return View(students);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var students = await _context.CompanySettingsDB.FindAsync(id);

            //delete image from wwwroot/Imgbannar
            var imgPath = Path.Combine(_WebHostEnvironment.WebRootPath, "images", students.Logo);
            if(System.IO.File.Exists(imgPath))
            System.IO.File.Delete(imgPath);


            _context.CompanySettingsDB.Remove(students);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}