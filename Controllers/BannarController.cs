using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data;
using Models.AdminModel;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace Adminpanel.Controllers
{
    public class BannarController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly IWebHostEnvironment _WebHostEnvironment;

        public BannarController(ApplicationContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _WebHostEnvironment = webHostEnvironment;
        }

        // GET: Bannar
        public async Task<IActionResult> Index()
        {
            return View(await _context.BannarDB.ToListAsync());
        }

        // GET: Bannar/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bannar = await _context.BannarDB
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bannar == null)
            {
                return NotFound();
            }

            return View(bannar);
        }

        // GET: Bannar/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Bannar/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Heading,SubTitle,ImageFile")] Bannar bannar)
        {
            if (ModelState.IsValid)
            {
                //Save image to wwwroot/imgBannar
                string wwwRootPath = _WebHostEnvironment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(bannar.ImageFile.FileName);
                string extension = Path.GetExtension(bannar.ImageFile.FileName);
                bannar.Image = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                string path = Path.Combine(wwwRootPath + "/ImgBannar/", fileName);
                using (var filestream = new FileStream(path, FileMode.Create))
                {
                    await bannar.ImageFile.CopyToAsync(filestream);
                }

                //Insert record

                _context.Add(bannar);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(bannar);
        }

        // GET: Bannar/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bannar = await _context.BannarDB.FindAsync(id);
            if (bannar == null)
            {
                return NotFound();
            }
            return View(bannar);
        }

        // POST: Bannar/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Heading,SubTitle,Image")] Bannar bannar)
        {
            if (id != bannar.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bannar);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BannarExists(bannar.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bannar);
        }

        // GET: Bannar/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bannar = await _context.BannarDB
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bannar == null)
            {
                return NotFound();
            }

            return View(bannar);
        }

        // POST: Bannar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bannar = await _context.BannarDB.FindAsync(id);

            //delete image from wwwroot/Imgbannar
            var imgPath = Path.Combine(_WebHostEnvironment.WebRootPath, "ImgBannar", bannar.Image);
            if(System.IO.File.Exists(imgPath))
            System.IO.File.Delete(imgPath);
            
            _context.BannarDB.Remove(bannar);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BannarExists(int id)
        {
            return _context.BannarDB.Any(e => e.Id == id);
        }
    }
}
