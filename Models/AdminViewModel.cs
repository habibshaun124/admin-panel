using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Models
{
    public class AdminViewModel
    {
        [Key]
        public int Id { get; set; }

        // [Required(ErrorMessage = "Please enter your Company name")]
        // [Display(Name = "Company Name")]

        public string CompanyName { get; set; }

        // [Required(ErrorMessage = "Please enter your address")]
        // [Display(Name = "Address")]
        public string Address { get; set; }

        // [Required(ErrorMessage = "Please enter your phone number")]
        // [Display(Name = "phone number")]

        public string PhoneNumber { get; set; }

        // [Required(ErrorMessage = "Please enter your email")]
        // [Display(Name = "Email address")]
        // [EmailAddress(ErrorMessage = "Please enter a valid email")]

        public string Email { get; set; }

        // [Required]
        public IFormFile Logo { get; set; }
    }
}