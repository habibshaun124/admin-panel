using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace Models.AdminModel
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        

        [Display(Name="Image")]
        public string Image { get; set; }

        [NotMapped]
        [Display(Name="Upload file")]
        public IFormFile ImageFile { get; set; }


        public string PreviewLink { get; set; }
        public virtual ICollection<ProductFeature> Features { get; set; }
    }
}