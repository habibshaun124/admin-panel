namespace Models.AdminModel
{
    public class ProductFeature
    {
        public int Id { get; set; }
        public string FeatureName { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}