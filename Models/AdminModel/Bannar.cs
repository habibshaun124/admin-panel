using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace Models.AdminModel
{
    public class Bannar
    {
        [Key]
        public int Id { get; set; }
        public string Heading { get; set; }
        public string SubTitle { get; set; }

        [DisplayName("Image")]
        public string Image { get; set; }
        
        [NotMapped]
        [DisplayName("Upload file")]
        public IFormFile ImageFile { get; set; }
    }
}